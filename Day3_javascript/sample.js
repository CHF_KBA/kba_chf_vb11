'use strict'
//Output Statement
console.log("Hello");
//Hello


//let & const 
let a
a = 20
console.log(a)
/*OUTPUT:
20*/

const a
a = 20
console.log(a)
//ERROR:Missing initializer in const declaration

//Object
const student =
{
    name: 'vimal',
    id: 's03',
    age: 23
}
console.log(student.name)

//OUTPUT: vimal

// JSON.stringify
const student =
{
    name: 'vimal',
    id: 's03',
    age: 23
}
const jsonString = JSON.stringify(student);
console.log(jsonString);

//JSON.parse

const jsonString = '{"name":"Vimal","id":"s03","age":23}';
const student = JSON.parse(jsonString);
console.log(student);



//method & arrow function
function square(number) {
    return number * number;
}
var x = square(2)

const square = (number) => number * number
var x = square(2)

//Import/Export
main.js
function add(x, y) {
    const a = x + y;
    return a
}
module.exports = { add }

main1.js
const main = require('./main');
console.log(main.add(3, 4))
//output: 7

//this
const student1 = {
    name: 'anil',
    age: 34,
    details: function () {
        console.log(name)
    }
}
student1.details()
//OUTPUT: unknown variable name



const student1 = {
    name: 'anil',
    age: 34,
    details: function () {
        console.log(student1.name)
    }
}
student1.details()

const student2 = {
    name: 'anna',
    age: 39,
    details: function () {
        console.log(student1.name)
    }
}
student2.details()
/*OUTPUT: anil
anil*/

const student1 = {
    name: 'anil',
    age: 34,
    details: function () {
        console.log(this.name)
    }
}
student1.details()

const student2 = {
    name: 'anna',
    age: 39,
    details: function () {
        console.log(this.name)
    }
}
student2.details()
/*OUTPUT: anil
anna*/

// Class

class Person {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    greet() {
        console.log(`Hello, my name is ${this.name} and I am ${this.age} years old.`);
    }
}
const person1 = new Person('John', 30);
const person2 = new Person('Jane', 25);

// Calling the 'greet' method on each instance
person1.greet();
person2.greet();



//Callback 
function greetUser(name, callback) {
    const greeting = `Hello, ${name}!`;
    callback(greeting);
}
function displayGreeting(message) {
    console.log(message);
}
greetUser('John', displayGreeting);




//Promise
const numbers = 3
let p = new Promise((resolve, reject) => {
    if (numbers > 0) {
        resolve('Positive')
    }
    else {
        reject('Negative')
    }
})
p.then((message) => {
    console.log('This is the message from then ' + message)
    console.log(numbers)
}).catch((message) => {
    console.log('This is the message from catch ' + message)
})

//async/await
console.log('Start')
function greet() {
    let p = new Promise((resolve, reject) => {
        setTimeout(() => resolve('Hello'), 1000)

    })

    let result = p
    console.log(result)
}
greet()
console.log('End')
/*Start
Promise { <pending> }
End*/

console.log('Start')
async function greet() {
    let p = new Promise((resolve, reject) => {
        setTimeout(() => resolve('Hello'), 1000)

    })

    let result = await p
    console.log(result)
}
greet()
console.log('End')
/*Start
End
Hello*/











