############## host terminal ##############

docker-compose -f docker/docker-compose-2org.yaml down

docker-compose -f docker/docker-compose-ca.yaml down

docker volume rm $(docker volume ls -q)

sudo rm -rf channel-artifacts/

sudo rm basic.tar.gz

sudo rm -rf organizations/

docker ps -a

// if there still exists the containers then execute the following commands.

docker rm $(docker container ls -q) --force

docker container prune

docker system prune

docker volume prune

docker network prune
