# KBA_CHF_VB11

Last date for submitting pitch deck: **4-Sep-2023**

Last date for submitting project: **27-Sep-2023**

Last date to complete all the Assessment tests (Assessment-1, Assessment-2, Assessment-3 and Assessment-4): **27-Sep-2023**

**Feedback Form**: https://forms.gle/HYVgJcHAhJVkNaRp7

**Hyperledger Fabric Certified Practitioner (HFCP)**: https://training.linuxfoundation.org/certification/hyperledger-fabric-certified-practitioner-hfcp/

**DWMS Registration**

**Kerala Knowledge Economy Mission (KKEM)** aims to create jobs for the educated population of Kerala. Over the next four years, the Kerala Knowledge Economy Mission aims to provide gainful employment to 20 lakh people in various sectors and job fields.

As we are associating with KKEM for **Placement Activities** for KBA Alumni,  learners need to register with the **Digital Workforce Management System(DWMS)** to avail of the services. 

Link to Platform: https://knowledgemission.kerala.gov.in/

After registering for the platform, learners need to provide a **DWMS ID** associated with their profile in the following form so that we can consider them for placement activities as part of KKEM. 

https://forms.gle/cTnBgdqkZsVRJd1X8


**Telegram link**: https://t.me/+1c7PQtoE5lw2MDFl

**Elearning course link**: https://elearning.kba.ai/courses/course-v1:KBA+CHFSPV23+2021SP/course/

**Hyperledger Fabric readthedocs**: https://hyperledger-fabric.readthedocs.io/en/release-2.5/

**Fabric samples**: https://github.com/hyperledger/fabric-samples


**Day17: UI**

Express Generator

`npx express-generator --view=hbs AutoApp`


**Day16: Rest API**

Postman installation : 

`sudo snap install postman`

App setup : 

`mkdir sampleApp`

`cd sampleApp`

`npm init -y`

`npm i express`

`npm i nodemon -D`


**Day14: Client**

`mkdir Client`

`cd Client`

`npm init -y`

`npm install @hyperledger/fabric-gateway`

Create profile.js, client.js, invokeTxn.js, privateTxn.js, queryTxn.js

`node invokeTxn.js`

`node privateTxn.js`

`node queryTxn.js`

**Day10: Fabric network**

**Cleanup**

`docker volume rm $(docker volume ls -q)`

`docker container prune`

`docker system prune`

`docker network prune`

`docker ps -a`

// if there still exists the containers then execute the following commands.

`docker rm $(docker container ls -q) --force`


**Day7: PDC**

**Inputs**

**Transaction arguments**

```
{
  "arg0": "Car01",
  "arg1": "Tata",
  "arg2": "Tiago",
  "arg3": "White",
  "arg4": "22/09/2022",
  "arg5": "F-01"
}
```
**Transient Data**

```
{
  "make": "Tata",
  "model": "Tiago",
  "color": "White",
  "dealerName": "Dealer-1"
}
```


**Minifab commands to deploy and invoke chaincode (If PDC is used)**

**Note**: Execute the following commands from **Network** folder

`./startNetwork.sh`

`sudo chmod -R 777 vars/`

`mkdir -p vars/chaincode/KBA-Automobile/node`

`cp -r ../Chaincode/KBA-Automobile/* vars/chaincode/KBA-Automobile/node/`

`cp vars/chaincode/KBA-Automobile/node/collections.json ./vars/KBA-Automobile_collection_config.json`

`minifab ccup -n KBA-Automobile -l node -v 3.0 -d false -r true`

`minifab invoke -n KBA-Automobile -p '"createCar","car01","Tata","Tiago","White","22/03/2023","F-01"'`

`minifab query -n KBA-Automobile -p '"readCar","car01"'`

```
MAKE=$(echo -n "Tata" | base64 | tr -d \\n)
MODEL=$(echo -n "Tiago" | base64 | tr -d \\n)
COLOR=$(echo -n "White" | base64 | tr -d \\n)
DEALER_NAME=$(echo -n "XXX" | base64 | tr -d \\n)
```

`minifab invoke -n KBA-Automobile -p '"OrderContract:createOrder","ord01"' -t '{"make":"'$MAKE'","model":"'$MODEL'","color":"'$COLOR'","dealerName":"'$DEALER_NAME'"}' -o dealer.auto.com`

`minifab query -n KBA-Automobile -p '"OrderContract:readOrder","ord01"'`

`minifab query -n KBA-Automobile -p '"queryAllCars"'`

`minifab query -n KBA-Automobile -p '"getCarsByRange","car01","car05"'`

`minifab query -n KBA-Automobile -p '"OrderContract:queryAllOrders"'`

`minifab query -n KBA-Automobile -p '"OrderContract:getOrdersByRange","ord01","ord05"'`

`minifab query -n KBA-Automobile -p '"getCarHistory","car01"'`

`minifab query -n KBA-Automobile -p '"getCarsWithPagination","10",""'`

`minifab query -n KBA-Automobile -p '"checkMatchingOrders","car01"'`

`minifab invoke -n KBA-Automobile -p '"matchOrder","car01","ord01"'`

`minifab invoke -n KBA-Automobile -p '"registerCar","car01","Bob","KL-01-XXXX"' -o mvd.auto.com`




**Day6: Minifab commands**

**Deploy chaincode using minifab**

`./startNetwork.sh`

`sudo chmod -R 777 vars/`

`mkdir -p vars/chaincode/KBA-Automobile/node`

`cp -r ../Chaincode/KBA-Automobile/* vars/chaincode/KBA-Automobile/node/`

`minifab ccup -n KBA-Automobile -l node -v 1.0 -d false`

`minifab invoke -n KBA-Automobile -p '"createCar","car01","Tiago"'`

`minifab query -n KBA-Automobile -p '"readCar","car01"'`

`minifab down`

`minifab restart`

`minifab query -n KBA-Automobile -p '"readCar","car01"'`

**Update the chaincode**

`cp -r ../Chaincode/KBA-Automobile/* vars/chaincode/KBA-Automobile/node/`

`minifab ccup -n KBA-Automobile -l node -v 2.0 -d false`

`minifab invoke -n KBA-Automobile -p '"createCar","car05","Tata","Tiago","White","22/06/2023","F-01"'`

`minifab query -n KBA-Automobile -p '"readCar","car05"'`

**To view explorer**

`minifab explorerup`

userid: exploreradmin

password: exploreradminpw

`minifab explorerdown`

**To view couchdb**

http://localhost:7006/_utils/

 userid: `admin`
 
 password: `adminpw`

**To view the details of a block**

`minifab blockquery`

`minifab blockquery -b 6`


**To view blockchain**

`docker exec -it peer1.mvd.auto.com /bin/sh`

`ls var/hyperledger/production/ledgersData/chains/chains/autochannel/`

`cat var/hyperledger/production/ledgersData/chains/chains/autochannel/blockfile_000000`

`exit`

**Day6: Chaincode Testing**

`npm run lint -- --fix`

`npm test`

**Day5: Chaincode**

**Start the network**

`./startNetwork.sh`

**Steps to deploy chaincode using IBM Blockchain Extension**

**For creating a new smart contract,**

- Under **SMART CONTRACTS** click the **three dots**(More Actions) 
- Select **Create New Project**. 
- Select **Default Contract** (or Private Data Contract to implement PDC)
- Select the language as **JavaScript**
- Enter a **name** for the asset(eg: Car)
- Click on the **Browse** option and **select a folder** (eg: Chaincode) where you want to save the chaincode (If needed, click on **Create Folder** and give a name for the folder(eg: KBA-Automobile))
- Click **save** and click **Open in current window**.
- The chaincode folder structure with the necessary files will be created.
- For adding business logic, edit the smart-contract available inside **lib** folder.

**To package chaincode,**

- Under **SMART CONTRACTS** click the **three dots**(More Actions) 
- Select **Package Open Project** (**Note**: Make sure that the root folder opened in VScode should be the chaincode folder(eg:KBA-Automobile) which contains the files such as package.json, index.js etc)
- Select **tar.gz**
- A package(eg: KBA-Automobile@0.0.1) will be created under SMART CONTRACTS

**To deploy chaincode,**

- Under **FABRIC ENVIRONMENTS**, expand the **channel** (eg: autochannel)
- Click on **Deploy smart contract**
- A new tab will be opened, under **Step 1** select the **package** ( eg: KBA-Automobile@0.0.1) to be deployed
- Click **Next**
- Under **Step 2**, if private smart contract then click **Add file** and navigate to the location that contains collections.json file, select  **collections.json** and click **Open**.
- Click **Next** and then click **Deploy**.
- Wait for a few minutes to deploy the chaincode. If successful, the chaincode will be listed under channel in FABRIC ENVIRONMENTS

**To invoke chaincode,**

- Under **FABRIC GATEWAYS**, click on a **gateway**.
- Expand the **channel** (eg: Autochannel)
- Expand the **chaincode** ( eg: KBA-Automobile@0.0.1) which lists the transactions.
- Click on one of the **transactions** (eg: createCar)
- A new tab will appear, enter the **Transaction arguments**, if private data then enter the **Transient data**.
- Click **Submit transaction** if updating the ledger or **Evaluate transaction** if querying the ledger.
- The output will be displayed under **Transaction output**.


**To upgrade chaincode,**

- Edit the **version** in **package.json**
- Follow the steps under **package** chaincode
- Follow the steps under **deploy** chaincode

**Develop Chaincode from scratch**

`mkdir SampleChaincode`

`cd SampleChaincode`

`npm init -y`

`npm i fabric-contract-api`

`npm i fabric-shim`

In package.json, add 
`"start": "fabric-chaincode-node start"`


**Day4: Minifab**

https://github.com/hyperledger-labs/minifabric

https://github.com/hyperledger-labs/minifabric/blob/main/spec.yaml


**Bring up the network**

`minifab netup -s couchdb -e true -i 2.4.8 -o manufacturer.auto.com`

`minifab create -c autochannel`

`minifab join -c autochannel`

`minifab anchorupdate`

`minifab profilegen -c autochannel`

**To view the containers**

`docker ps -a`

**Steps to add wallets in IBM Blockchain Extension**

- Under **FABRIC WALLETS** click **+  (Add Wallet)**
- Select **Specify an existing file system wallet**
- Click **Browse** and navigate to **Network -> var -> profiles -> vscode -> wallets** 
- Select the **organization** folder(eg: manufacturer.auto.com) and click **Select** button to add the wallet of the organization.
- **Successfully added a wallet** message will pop up.
- Repeat the process to add the wallets of other organizations.

**Steps to add environments in IBM Blockchain Extension**

- Under **FABRIC ENVIRONMENTS** click  **+  ( Add Environment)**
- Select **Add any other Fabric Network**
- Enter a **name** for the Environment(eg:Automobile)
- Click **Browse** and navigate to **Network -> vars -> profiles -> vscode -> vscodenodefile.json** and click **Select** button
- Select **Done adding nodes**.
- **Click** on the newly created Fabric Environment(eg: Automobile) and it lists the ca’s of each organization
- **Click on a ca**, select **Choose an existing identity**, Select **wallet** of that organization, Select **Admin** and select **No**. Repeat this process for the other ca’s.        

**Steps to add gateways in IBM Blockchain Extension**

- Under **FABRIC GATEWAYS** click  + (Add Gateway)
- Select **Create a  gateway from a Fabric environment**
- Choose the **environment** (eg: Automobile)
- Enter a **name** for the gateway (eg: manufacturer_gw)
- Choose the **organisation** (eg: manufacturer-auto-com) for which the gateway is created 
- Choose the **certificate authority** of the organization ( eg:  ca1.manufacturer.auto.com)
- Repeat the process for all the Organizations.

**Steps to disconnect the environment in IBM Blockchain Extension**

- Delete the gateways Under **FABRIC GATEWAYS**, click **Disconnect** from Gateway, right-click the gateway, select **Delete gateway** and click **Yes** to confirm
- Delete the Fabric environment under **FABRIC ENVIRONMENTS**, click **Disconnect** from a Fabric Environment, right-click the environment, select **Delete** and click **Yes** to confirm.
- Delete the wallets under  **FABRIC WALLETS**,  right-click the wallet, select **Remove** and click **Yes** to confirm.

**Bring down the network**

`minifab cleanup`

**Using script**

`chmod +x startNetwork.sh`

`./startNetwork.sh`

**To remove the containers**

`docker container prune`

`docker system prune`

`docker volume prune`

`docker network prune`

**To check for containers**

`docker ps -a`

**To forcefully remove containers**

`docker rm $(docker container ls -q) --force`

**To check for volumes**

`docker volume ls`

**To delete the volumes**

`docker volume rm $(docker volume ls -q)`

**To delete vars folder**

`sudo rm -rf vars`


**Day3: Test Network**

**Download fabric-samples**

`curl -sSLO https://raw.githubusercontent.com/hyperledger/fabric/main/scripts/install-fabric.sh && chmod +x install-fabric.sh`


`./install-fabric.sh -f '2.5.3' -c '1.5.6'`

`sudo cp fabric-samples/bin/* /usr/local/bin`

`cd fabric-samples/test-network/`

**Test Network commands**

`./network.sh -h`

`./network.sh up createChannel`

`docker ps --format "table {{.Image}}\t{{.Ports}}\t{{.Names}}"`

**Deploy Chaincode Command**

`./network.sh deployCC -ccn basic -ccp ../asset-transfer-basic/chaincode-javascript/ -ccl javascript`

**Set up Environment Variables**

`export FABRIC_CFG_PATH=$PWD/../config/`
`export CORE_PEER_TLS_ENABLED=true`
`export CORE_PEER_LOCALMSPID="Org1MSP"`

`export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt`

`export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp`

`export CORE_PEER_ADDRESS=localhost:7051`

**Invoke Chaincode**

`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile "${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem" -C mychannel -n basic --peerAddresses localhost:7051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" --peerAddresses localhost:9051 --tlsRootCertFiles "${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" -c '{"function":"InitLedger","Args":[]}'`

**Query chaincode**

`peer chaincode query -C mychannel -n basic -c '{"Args":["GetAllAssets"]}'`

`peer chaincode query -C mychannel -n basic -c '{"function":"ReadAsset","Args":["asset5"]}'`

**Bring down network**

`./network.sh down`

 `docker system prune`
 

**Day2: Install Dependencies**

`sudo apt update`

**CURL**

`sudo apt install curl -y`

**Docker**

`curl -fsSL https://get.docker.com -o get-docker.sh`

`chmod +x get-docker.sh`

`./get-docker.sh`

`rm get-docker.sh`

**To enable your user to properly use the docker commands without using sudo for every command, execute the following command:**

`sudo usermod -aG docker $USER`

**Restart your laptop**

**Docker-compose**

`sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

`sudo chmod +x /usr/local/bin/docker-compose`

**JQ**

`sudo apt install jq -y`

**NodeJS and npm**

`curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -`

`sudo apt install -y nodejs`

`node -v`

`npm -v`

**Minifab**

`curl -o minifab -sL https://tinyurl.com/yxa2q6yr && chmod +x minifab`

`sudo cp minifab /usr/local/bin`

`minifab`

**Build-Essentials**

`sudo apt install build-essential`

**VSCODE**

`sudo snap install --classic code`

**IBM Blockhain Extension**

Please download the IBM Blockchain Platform Extension for VS Code from gitlab and then follow these steps:

- Open VScode and click on Extensions tab.
- Click on Views and More Actions(three dots at the top).
- Select Install from VSIX.
- Browse to the location that contains the downloaded file, ibm-blockchain-platform-2.0.8.vsix, select it and click Install.
- After successful installation, you will get an alert and also the icon will be available in VScode.

**Day2: Docker**

`docker build -t image1:1.0 .`

`docker run image1:1.0`

`docker-compose up -d`

`docker-compose config`

http://localhost:3000/_utils

Volume Mapping

`sudo chmod -R 777 samplefolder`

`docker ps -a`

`docker exec -it <container ID> bash`

`ls /hyperledger/fabric/newfolder`

`cat /hyperledger/fabric/newfolder/<file_name>`

`docker-compose down`

**To remove the containers**

`docker container prune`

`docker system prune`

`docker volume prune`

`docker network prune`


**Day1: Introduction**

Hyperledger Projects: https://dltlandscape.hyperledger.org/projects

Raft: http://thesecretlivesofdata.com/raft/

