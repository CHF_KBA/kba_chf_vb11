let profile = {
    manufacturer: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/manufacturer.auto.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json",
        "tlsCertPath":"../Network/vars/keyfiles/peerOrganizations/manufacturer.auto.com/tlsca/tlsca1.manufacturer.auto.com-cert.pem",
        "peerEndpoint": "localhost:7003",
        "peerHostAlias": "peer1.manufacturer.auto.com",
        "mspId": "manufacturer-auto-com"


    },
    dealer: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/dealer.auto.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json",
        "tlsCertPath": "../Network/vars/keyfiles/peerOrganizations/dealer.auto.com/tlsca/tlsca1.dealer.auto.com-cert.pem",
        "peerEndpoint": "localhost:7004",
        "peerHostAlias": "peer1.dealer.auto.com",
        "mspId": "dealer-auto-com"
    },
    mvd: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/mvd.auto.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json",
        "tlsCertPath":"../Network/vars/keyfiles/peerOrganizations/mvd.auto.com/tlsca/tlsca1.mvd.auto.com-cert.pem",
        "peerEndpoint": "localhost:7005",
        "peerHostAlias": "peer1.mvd.auto.com",
        "mspId": "mvd-auto-com"
    }
}
module.exports = { profile }
