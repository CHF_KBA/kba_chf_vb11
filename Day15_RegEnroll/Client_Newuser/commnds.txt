//Within the Automobile folder

mkdir Client_Newuser 

cd Client_Newuser

npm init -y

npm install @hyperledger/fabric-gateway

npm install fabric-ca-client@2.2.4

npm install fabric-network@2.2.4

npm install prompt-sync


//Create profile_User.js, enrollAdmin.js,registerUser.js, client_User.js, invokeTxn.js,queryTxn.js

node enrollAdmin.js

node registerUser.js

// when you execute invokeTxn.js & queryTxn.js files give input as User1

node invokeTxn.js

node queryTxn.js


