const { profile } = require('./profile_User')
const { promises: fs } = require('fs')
const { Wallets } = require('fabric-network')
const path = require('path');
const crypto = require('crypto')
const grpc = require('@grpc/grpc-js');
const { connect, signers } = require('@hyperledger/fabric-gateway');
const prompt = require('prompt-sync')();

class ClientApplication {
    async submitTxn(organization, channelName, chaincodeName, contractName, txnType, transientData, txnName, ...args) {
        let orgProfile = profile[organization];
        const client = await newGrpcConnection(orgProfile["tlsCertPath"], orgProfile["peerEndpoint"], orgProfile["peerHostAlias"]);
        const wallet = await Wallets.newFileSystemWallet(orgProfile["Wallet"])
        const User_name = prompt('Enter the username you wish to retrieve information for:');
        const identity = await wallet.get(User_name);
        if(identity)
        {
        //console.log('identity=',identity)
        const credentials_new=identity["credentials"]
        const pemPrivateKey=credentials_new["privateKey"]
        const publickey=credentials_new["certificate"]
        

        const gateway = connect({
            client,
            identity: await newIdentity(publickey, orgProfile["mspId"]),
            signer: await newSigner(pemPrivateKey)

        });
        
        try {
            let network = await gateway.getNetwork(channelName);
            let contract = await network.getContract(chaincodeName, contractName);
            let resultBytes;

            if (txnType == "invokeTxn") {
                resultBytes = await contract.submitTransaction(txnName, ...args)
            } else if (txnType == "queryTxn") {
                resultBytes = await contract.evaluateTransaction(txnName, ...args)
            } else if (txnType == "privateTxn") {
                await contract.submit(txnName, { arguments: [...args], transientData: transientData, },);
            } else {
                console.log("Invalid txnType", txnType);
            }

            console.log('*** Result:', resultBytes);

            return Promise.resolve(resultBytes)

        } catch (error) {
            console.log("Error occured", error)
            return Promise.reject(error);
        } finally {
            gateway.close();
            client.close();
        }
    }
    else
    {
        console.log("No User Exist")
    }
}


}

async function newGrpcConnection(tlsCertPath, peerEndpoint, peerHostAlias) {
    const tlsRootCert = await fs.readFile(tlsCertPath);
    const tlsCredentials = grpc.credentials.createSsl(tlsRootCert)
    return new grpc.Client(peerEndpoint, tlsCredentials, { 'grpc.ssl_target_name_override': peerHostAlias });

}

async function newIdentity(publickey, mspId) {
    const credentials = Buffer.from(publickey);
    return { mspId, credentials }
}

async function newSigner(pemPrivateKey) {
    
    const privateKeyPem = Buffer.from(pemPrivateKey)
    const privateKey = crypto.createPrivateKey(privateKeyPem);
    return signers.newPrivateKeySigner(privateKey);
}

module.exports = { ClientApplication }

