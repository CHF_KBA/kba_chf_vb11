const express = require("express");
const app = express();
const port = 3000;
const { ClientApplication } = require("../Client/client");
let userClient = new ClientApplication();

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/api/car/:carId", (req, res) => {
  userClient
    .submitTxn(
      "manufacturer",
      "autochannel",
      "KBA-Automobile",
      "CarContract",
      "queryTxn",
      "",
      "readCar",
      `${req.params.carId}`
    )
    .then((result) => {
      console.log(new TextDecoder().decode(result));
      res.send(new TextDecoder().decode(result));
    });
});

app.post("/api/car", (req, res) => {
  userClient
    .submitTxn(
      "manufacturer",
      "autochannel",
      "KBA-Automobile",
      "CarContract",
      "invokeTxn",
      "",
      "createCar",
      `${req.body.carId}`,
      `${req.body.make}`,
      `${req.body.model}`,
      `${req.body.color}`,
      `${req.body.dateOfManufacture}`,
      `${req.body.manufacturerName}`
    )
    .then((result) => {
      console.log(new TextDecoder().decode(result));
      res.send("Car Created");
    });
});

//   {
//     "carId":"car007",
//     "make":"Aston Martin",
//     "model":"Vanquish", "color":"Black", "dateOfManufacture":"12/12/20", "manufactureName":"Aston Martin"
//  }

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
