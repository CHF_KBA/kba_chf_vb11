const { ClientApplication } = require('./client')

let userClient = new ClientApplication();

const transientData = {
    make: Buffer.from("Tata"),
    model: Buffer.from("Nexon"),
    color: Buffer.from("White"),
    dealerName: Buffer.from("Dealer 2")
}

userClient.submitTxn(
    "dealer",
    "autochannel",
    "KBA-Automobile",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-05",
).then(result => {
    console.log(new TextDecoder().decode(result))
})