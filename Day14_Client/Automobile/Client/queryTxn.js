const { ClientApplication } = require('./client')

let userClient = new ClientApplication()
userClient.submitTxn(
    "manufacturer",
    "autochannel",
    "KBA-Automobile",
    "CarContract",
    "queryTxn",
    "",
    "readCar",
    "Car-03",
).then(result => {
    console.log(new TextDecoder().decode(result))
});

userClient.submitTxn(
    "dealer",
    "autochannel",
    "KBA-Automobile",
    "OrderContract",
    "queryTxn",
    "",
    "readOrder",
    "Order-05",
).then(result => {
    console.log(new TextDecoder().decode(result))
});

